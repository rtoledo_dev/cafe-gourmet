class Produto < ActiveRecord::Base
  scope :recentes, order('created_at DESC')
  scope :antigos, order('created_at ASC')
  scope :ativos, where(:ativo => true)
  scope :destaques, where(:destaque_inicial => true)
  validates :nome, :presence => true
  validates :categoria_id, :presence => true
  validates :valor, :presence => true
  validates :descricao, :presence => true
  validates :peso, :presence => true
  usar_como_dinheiro :valor, :valor_promocional
  belongs_to :categoria
  has_many :imagem_produtos, :class_name => "ImagemProduto"  
  has_attached_file :capa, :styles => { 
    :medium => "179x201",
    :large => "260x260", 
    :thumb => "75x75#",
    :mini => '45x45#'}
    
  validates_attachment_presence :capa
  
  accepts_nested_attributes_for :imagem_produtos
  
  def valor_ou_promocao
    self.valor_promocional.to_f <= 0 ? self.valor : self.valor_promocional
  end

  def to_param
    [id,nome].compact.join('-').parameterize
  end
  
end
