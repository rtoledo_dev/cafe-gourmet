class Banner < ActiveRecord::Base
  validates :descricao, :presence => true
	has_attached_file :imagem, :styles => { 
    :medium => "940x258!", 
    :thumb => "75x75#"},
  :default_style => :medium
  validates_attachment_presence :imagem
  
end
