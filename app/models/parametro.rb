class Parametro < ActiveRecord::Base
  usar_como_dinheiro :taxa_embalagem

  validates_presence_of :taxa_embalagem

  def self.first_or_create
    self.first || self.new(:taxa_embalagem => 12)
  end
end
