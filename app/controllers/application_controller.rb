# encoding: utf-8
class ApplicationController < ActionController::Base
  protect_from_forgery
  
  before_filter :carregar_carrinho
  helper_method :current_user, :logged?, :carrinho_usuario, :nome_produto
  
  def current_user
    return nil unless logged?
    @current_user ||= Usuario.find(session[:current_user]['id'])
  end
  
  def check_logged
    return if controller_name == "login"
    unless logged?
      flash[:error] = 'Você não está autenticado no sistema'
      redirect_to entrar_path
    end
  end
  
  def logged?
    !session[:current_user].nil?
  end

  def nome_produto(produto_id, truncate_nome = 13)
    if truncate_nome
      Produto.find(produto_id).nome[0,truncate_nome]+'...' rescue ''
    else
      Produto.find(produto_id).nome
    end
  end
  
  
  protected
  def carrinho_usuario
    session[:carrinho]
  end
  
  def redirect_if_logged
    redirect_to principal_path if logged?
  end
  
  def carregar_carrinho
    session[:carrinho] ||= []
  end
end
