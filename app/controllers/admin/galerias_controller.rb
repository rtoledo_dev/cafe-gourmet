class Admin::GaleriasController < Admin::AdminController
  uses_tiny_mce(:options => AppConfig.default_mce_options, :only => [:new, :edit])
  # GET /admin/galerias
  # GET /admin/galerias.xml
  def index
    @admin_galerias = Galeria.paginate(:page => params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @admin_galerias }
    end
  end

  # GET /admin/galerias/1
  # GET /admin/galerias/1.xml
  def show
    @admin_galeria = Galeria.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @admin_galeria }
    end
  end

  # GET /admin/galerias/new
  # GET /admin/galerias/new.xml
  def new
    @admin_galeria = Galeria.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @admin_galeria }
    end
  end

  # GET /admin/galerias/1/edit
  def edit
    @admin_galeria = Galeria.find(params[:id])
  end

  # POST /admin/galerias
  # POST /admin/galerias.xml
  def create
    @admin_galeria = Galeria.new(params[:galeria])

    respond_to do |format|
      if @admin_galeria.save
        # sleep 1
        format.html { redirect_to(admin_galerias_path, :notice => 'Galeria foi criado(a) corretamente.') }
        format.xml  { render :xml => @admin_galeria, :status => :created, :location => @admin_galeria }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @admin_galeria.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/galerias/1
  # PUT /admin/galerias/1.xml
  def update
    @admin_galeria = Galeria.find(params[:id])

    respond_to do |format|
      if @admin_galeria.update_attributes(params[:galeria])
        # sleep 1
        format.html { redirect_to(admin_galerias_path, :notice => 'Galeria foi atualizado(a) corretamente.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @admin_galeria.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/galerias/1
  # DELETE /admin/galerias/1.xml
  def destroy
    @admin_galeria = Galeria.find(params[:id])
    @admin_galeria.destroy

    respond_to do |format|
      # sleep 1
      format.html { redirect_to(admin_galerias_url) }
      format.xml  { head :ok }
    end
  end
end
