class Admin::BannersController < Admin::AdminController
  # GET /admin/banners
  # GET /admin/banners.xml
  def index
    @admin_banners = Banner.paginate(:page => params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @admin_banners }
    end
  end

  # GET /admin/banners/1
  # GET /admin/banners/1.xml
  def show
    @admin_banner = Banner.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @admin_banner }
    end
  end

  # GET /admin/banners/new
  # GET /admin/banners/new.xml
  def new
    @admin_banner = Banner.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @admin_banner }
    end
  end

  # GET /admin/banners/1/edit
  def edit
    @admin_banner = Banner.find(params[:id])
  end

  # POST /admin/banners
  # POST /admin/banners.xml
  def create
    @admin_banner = Banner.new(params[:banner])

    respond_to do |format|
      if @admin_banner.save
        # sleep 1
        format.html { redirect_to(admin_banners_url, :notice => 'Banner foi criado(a) corretamente.') }
        format.xml  { render :xml => @admin_banner, :status => :created, :location => @admin_banner }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @admin_banner.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/banners/1
  # PUT /admin/banners/1.xml
  def update
    @admin_banner = Banner.find(params[:id])

    respond_to do |format|
      if @admin_banner.update_attributes(params[:banner])
        # sleep 1
        format.html { redirect_to(admin_banners_url, :notice => 'Banner foi atualizado(a) corretamente.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @admin_banner.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/banners/1
  # DELETE /admin/banners/1.xml
  def destroy
    @admin_banner = Banner.find(params[:id])
    @admin_banner.destroy

    respond_to do |format|
      # sleep 1
      format.html { redirect_to(admin_banners_url) }
      format.xml  { head :ok }
    end
  end
end
