class Admin::ParametrosController < Admin::AdminController
  # GET /admin/parametros
  # GET /admin/parametros.xml
  def index
    @admin_parametros = Parametro.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @admin_parametros }
    end
  end

  # GET /admin/parametros/1
  # GET /admin/parametros/1.xml
  def show
    @admin_parametro = Parametro.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @admin_parametro }
    end
  end

  # GET /admin/parametros/new
  # GET /admin/parametros/new.xml
  def new
    @admin_parametro = Parametro.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @admin_parametro }
    end
  end

  # GET /admin/parametros/1/edit
  def edit
    @admin_parametro = Parametro.first_or_create
  end

  # POST /admin/parametros
  # POST /admin/parametros.xml
  def create
    @admin_parametro = Parametro.first_or_create

    respond_to do |format|
      if @admin_parametro.save
        format.html { redirect_to(edit_admin_parametro_path(:atual), :notice => 'Parametros foram salvos com sucesso') }
        format.xml  { render :xml => @admin_parametro, :status => :created, :location => @admin_parametro }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @admin_parametro.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/parametros/1
  # PUT /admin/parametros/1.xml
  def update
    @admin_parametro = Parametro.first_or_create

    respond_to do |format|
      if @admin_parametro.update_attributes(params[:parametro])
        format.html { redirect_to(edit_admin_parametro_path(:atual), :notice => 'Parametros foram salvos com sucesso') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @admin_parametro.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/parametros/1
  # DELETE /admin/parametros/1.xml
  def destroy
    @admin_parametro = Parametro.find(params[:id])
    @admin_parametro.destroy

    respond_to do |format|
      format.html { redirect_to(admin_parametros_url) }
      format.xml  { head :ok }
    end
  end
end
