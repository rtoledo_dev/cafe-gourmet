class Admin::ImagemGaleriasController < Admin::AdminController
 
  # DELETE /admin/imagem_galerias/1
  # DELETE /admin/imagem_galerias/1.xml
  def destroy
    @admin_imagem_galeria = ImagemGaleria.find(params[:id])
    @admin_imagem_galeria.destroy

    respond_to do |format|
      # sleep 1
      format.html { redirect_to(edit_admin_galeria_path(params[:galeria_id]),:notice=>'Imagem removida com sucesso') }
      format.xml  { head :ok }
    end
  end
end
