# encoding: utf-8
class Admin::LoginController < Admin::AdminController
  layout 'admin/login'
  def index
    
  end
  
  def logout
    session[:admin_current_user] = nil
    redirect_to admin_login_path
  end
  
  def enter
    usuario = Usuario.where(:email => params[:email], :senha => params[:senha]).first
    if usuario
      session[:admin_current_user] = usuario.attributes
      redirect_to admin_root_path
    else
      flash[:error] = 'Erro com usuário/senha'
      render :index
    end
  end
end
