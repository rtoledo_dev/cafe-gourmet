class Admin::CategoriasController < Admin::AdminController
  # GET /admin/categorias
  # GET /admin/categorias.xml
  def index
    @admin_categorias = Categoria.paginate(:page => params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @admin_categorias }
    end
  end

  # GET /admin/categorias/1
  # GET /admin/categorias/1.xml
  def show
    @admin_categoria = Categoria.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @admin_categoria }
    end
  end

  # GET /admin/categorias/new
  # GET /admin/categorias/new.xml
  def new
    @admin_categoria = Categoria.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @admin_categoria }
    end
  end

  # GET /admin/categorias/1/edit
  def edit
    @admin_categoria = Categoria.find(params[:id])
  end

  # POST /admin/categorias
  # POST /admin/categorias.xml
  def create
    @admin_categoria = Categoria.new(params[:categoria])

    respond_to do |format|
      if @admin_categoria.save
        # sleep 1
        format.html { redirect_to(admin_categorias_url, :notice => 'Categoria foi criado(a) corretamente.') }
        format.xml  { render :xml => @admin_categoria, :status => :created, :location => @admin_categoria }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @admin_categoria.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /admin/categorias/1
  # PUT /admin/categorias/1.xml
  def update
    @admin_categoria = Categoria.find(params[:id])

    respond_to do |format|
      if @admin_categoria.update_attributes(params[:categoria])
        # sleep 1
        format.html { redirect_to(admin_categorias_url, :notice => 'Categoria foi atualizado(a) corretamente.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @admin_categoria.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/categorias/1
  # DELETE /admin/categorias/1.xml
  def destroy
    @admin_categoria = Categoria.find(params[:id])
    @admin_categoria.destroy

    respond_to do |format|
      # sleep 1
      format.html { redirect_to(admin_categorias_url) }
      format.xml  { head :ok }
    end
  end
end
