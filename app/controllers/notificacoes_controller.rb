class NotificacoesController < ApplicationController
	skip_before_filter :verify_authenticity_token

  def salvar
  	email = "pagseguro@cafegourmetbrasil.com"
    token = "8FBB7945FDB048A1AE0BB428CCDB0CDA"
    notification_code = params[:notificationCode]

    notification = PagSeguro::Notification.new(email, token, notification_code)

    pedido = Pedido.find(notification.id)
    if pedido.pagseguro_id.blank?
      attributes = {
        :total => notification.gross_amount,
        :pagseguro_id => notification.transaction_id,
        :entrega_cep => notification.shipping.postal_code,
        :entrega_estado => notification.shipping.state,
        :entrega_cidade => notification.shipping.city,
        :entrega_endereco => notification.shipping.street,
        :entrega_numero => notification.shipping.number,
        :entrega_complemento => notification.shipping.complement,
        :entrega_bairro => notification.shipping.district
        # :pagseguro_observacoes => notification.shipping.to_hash
      }


      pedido.update_attributes(attributes)
    end

    

    if notification.approved?
      # Idealmente faça alguns testes de sanidade, como notification.gross_amount, notification.item_count, etc
      # notification.id referencia o id do payment/invoice, caso tenha sido configurado
      # transacation_id identifica o código da transação no pag seguro
      pedido.update_attribute(:status, 'Aprovado')
    end

    if notification.cancelled?
      pedido.update_attribute(:status, 'Cancelado')
    end

  	render :nothing => true
  end

end
