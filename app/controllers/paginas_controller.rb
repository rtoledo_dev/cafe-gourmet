class PaginasController < ApplicationController
  def index
  	@pagina = Pagina.find_by_url(params[:pagina_url])
  	raise unless @pagina
    @galerias = Galeria.all
  rescue
  	redirect_to root_path
  end

end
