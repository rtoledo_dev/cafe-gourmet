class FaleConoscoController < ApplicationController
  def index
  end
  
  def enviar_contato
    [
      :nome, :email, :assunto, :mensagem
    ].each do |p|
      if params[p].blank?
        flash[:error] = 'Preencha corretamente os campos'
        render :index
        return
      end
    end
    Contato.fale_conosco(params[:assunto], params[:nome], params[:email], params[:pedido], params[:mensagem]).deliver

    redirect_to root_path, :notice => 'Mensagem enviada com sucesso. Em breve retornaremos seu contato'
  end

end
