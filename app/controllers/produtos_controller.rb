class ProdutosController < ApplicationController
  def index
    @produtos = []
    unless params[:q].blank?
      @produtos = Produto.ativos.recentes.where('nome LIKE ? OR descricao LIKE ?',"%#{params[:q]}%","%#{params[:q]}%")
    else
      @produtos = Produto.ativos.recentes
    end
  end

  def detalhes
    @produto = Produto.find(params[:id])
  end

end
