# encoding: utf-8
class CarrinhoController < ApplicationController
  before_filter :checar_produto, :only => [:adicionar, :diminuir, :remover]

  def index
    
    unless carrinho_usuario.blank?
      # raise carrinho_usuario.inspect
      # adicionando os produtos do pedido ao objeto do formulario
      carrinho_usuario.each_with_index do |item,i|
        unless Produto.exists?(item[:id])
          carrinho_usuario[i] = nil
          next
        end
      end
      carrinho_usuario.compact!
    end
  end
  
  def adicionar
    adicionar_quantidade(params[:produto_id])
    redirect_to carrinho_path, :notice => 'Produto adicionado com sucesso'
  rescue
    flash[:error] = 'Erro ao adicionar o produto ao carrinho de compras'
    redirect_to root_path
  end
  
  def diminuir
    subtrair_quantidade(params[:produto_id])
    redirect_to carrinho_path, :notice => 'Produto diminuído com sucesso'
  rescue
    flash[:error] = 'Erro ao diminuir o produto do carrinho de compras'
    redirect_to root_path
  end
  
  def remover
    remover_produto(params[:produto_id])
    redirect_to carrinho_path, :notice => 'Produto removido com sucesso'
  rescue
    flash[:error] = 'Erro ao remover o produto do carrinho de compras'
    redirect_to root_path
  end

  def efetuar_pagamento
    @pedido = Pedido.new(params[:pedido])
    unless @pedido.save
      flash.now[:error] = 'Preencha os campos corretamente'
      render :action => :dados_cliente
    else
      @order = PagSeguro::Payment.new('pagseguro@cafegourmetbrasil.com', '8FBB7945FDB048A1AE0BB428CCDB0CDA', :id => @pedido.id)
      @order.extra_amount = sprintf("%.02f",carrinho_usuario.sum{|t| t[:qtd] * Parametro.first_or_create.taxa_embalagem})
      @order.sender = PagSeguro::Sender.new(:name => @pedido.nome, :email => @pedido.email)
      carrinho_usuario.each_with_index do |item,i|
        @order.items << PagSeguro::Item.new(:id => item[:id], 
          :amount => sprintf("%.02f",Produto.find(item[:id]).valor_ou_promocao.to_f), 
          :weight => (Produto.find(item[:id]).peso.to_f * 100).to_i,
          :quantity => item[:qtd],
          :description => nome_produto(item[:id], false))
      end
      carrinho_usuario.compact!
      @order.shipping = PagSeguro::Shipping.new(type: PagSeguro::Shipping::PAC)

      # raise @pedido.inspect
      redirect_to @order.checkout_payment_url
    end

    
    
  end

  def dados_cliente
    @pedido = Pedido.new
  end
  
  protected
  
  def adicionar_quantidade(produto_id)
    produto = Produto.find(produto_id)
    carrinho_usuario.each_with_index do |item,i|
      if item[:id].to_i == produto.id
        item[:qtd] = item[:qtd] + 1
        item[:valor] = produto.valor_ou_promocao.to_f
        carrinho_usuario[i] = item
        return
      end
    end
    carrinho_usuario << {:id => produto_id.to_i, :qtd => 1, :valor => produto.valor_ou_promocao.to_f}
  end
  
  def subtrair_quantidade(produto_id)
    produto = Produto.find(produto_id)
    carrinho_usuario.each_with_index do |item,i|
      if item[:id].to_i == produto.id
        item[:qtd] = item[:qtd] - 1
        if item[:qtd] <= 0
          carrinho_usuario[i] = nil
          carrinho_usuario.compact!
          return
        end
        
        item[:valor] = produto.valor_ou_promocao.to_f
        carrinho_usuario[i] = item
        return
      end
    end
  end
  
  def remover_produto(produto_id)
    produto = Produto.find(produto_id)
    carrinho_usuario.each_with_index do |item,i|
      if item[:id].to_i == produto.id
        carrinho_usuario[i] = nil
        carrinho_usuario.compact!
        return
      end
    end
  end
  
  def checar_produto
    raise 'produto inexistente' unless Produto.exists?(params[:produto_id])
  end
end
