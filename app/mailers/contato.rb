class Contato < ActionMailer::Base
  default :from => "admin@cafegourmetbrasil.com", :cc => ['rodrigo@altitudeag.com.br']

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contato.fale_conosco.subject
  #
  def fale_conosco(assunto, nome, email, pedido, mensagem)
    @pedido = pedido
    @mensagem = mensagem
    @nome = nome
    @email = email

    mail :to => "admin@cafegourmetbrasil.com", :reply_to => email, :subject => "Contato feito pelo site - #{assunto}"
  end
end
