module Admin::ProdutosHelper
  
  
  def imagem_produto(produto_id,definicao = :mini)
    image_tag(Produto.find(produto_id).capa.url(definicao)) rescue ''
  end
  
  def preco_produto(preco)
    preco.to_f.real_contabil
  end
  
  def link_to_add_fields(name, f, association, link_options = {})
    link_options = {:class => "add_fields"}.merge(link_options)
    new_object = f.object.send(association).new
    logger.info { new_object.object_id.inspect }
    id = new_object.object_id
    fields = f.fields_for(association, new_object, :child_index => id) do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    logger.info { "-----------" }
    logger.info { fields.inspect }
    link_to(name, '#', link_options.merge('data-id' => id, 'data-fields' => escape_javascript(fields)))
  end
end
