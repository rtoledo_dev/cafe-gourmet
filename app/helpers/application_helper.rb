module ApplicationHelper
  def users_options_for_current
    users = User.order(:name).all.collect{|t| [t.name,t.id]}
    options_for_select(users,current_user.id)
  end
  
  def flash_messages
    output = []
    flash.each do |type,message|
      output << content_tag(:h4, message, :class => "alert_#{type}")
    end
    raw(output.join)
  end
  
  def flash_messages_front
    output = []
    flash.each do |type,message|
      output << content_tag(:div, message, :class => "alert_#{type}")
    end
    raw(output.join)
  end
  
  def header_links
    output = []
    output << link_to('Principal',root_path)
    output << link_to('Produtos',produtos_path)
    output << link_to('Carrinho de compras',carrinho_path)
    output << link_to('Fale conosco',fale_conosco_path)
    # unless logged?
    #       output << link_to('Registrar', usuarios_registrar_path)
    #       output << link_to('Entrar', entrar_path)
    #     else
    #       output << link_to('Sair', usuarios_sair_path)
    #       output << link_to('Seus álbuns', albuns_path)
    #     end
    #     output << link_to('15 anos', index_15_anos_path)
    #     output << link_to('Fale conosco', fale_conosco_path)
    
    output = output.collect{|t| content_tag(:li,t.html_safe)}
    
    # content_tag(:ul,output.join(content_tag(:li,image_tag('top-menu-separator.jpg'),:class => 'separator')).html_safe)
    content_tag(:ul,output.join.html_safe, :id => 'header_links')
  end
  
  def footer_links_grouped
  	links = Pagina.all.collect{|t| {:texto => t.titulo, :url => pagina_path(t.url)}}
  	links << {:texto => 'Fale conosco', :url => fale_conosco_path}
  	
  	links.in_groups_of(3,false)
  end
end
