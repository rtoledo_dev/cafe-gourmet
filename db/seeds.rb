# encoding: utf-8
Usuario.create!(:email => 'administrador@email.com.br',
:nome => 'Administrador',
:senha => 'raposa')



1.upto(5) do
  Categoria.create(:nome => "#{Random.firstname} #{Random.lastname} #{Random.lastname}")
end


[
  'slide_00.jpg','slide_01.jpg','slide_02.jpg'
].each do |image|
  Banner.create!(:descricao => "#{Random.firstname} #{Random.lastname} #{Random.lastname}",
  :url => 'http://www.agenciainvent.com.br',
  :imagem => File.open(File.join(Rails.root,'db','images',image)))
end

1.upto(2) do
  1.upto(6) do |i|
    1.upto(2) do
      produto = Produto.create!(:nome => "#{Random.firstname} #{Random.lastname} #{Random.lastname}",
      :valor => Random.number(999), :descricao => Random.paragraphs(3),
      :categoria_id => Categoria.first.id,
      :ativo => true,
      :destaque_inicial => true,
      :capa => File.open(File.join(Rails.root,'db','images','produtos',"#{i}-home.jpg")))
      1.upto(3) do |j|
        produto.imagem_produtos.build(:arquivo => File.open(File.join(Rails.root,'db','images','produtos',"#{j}-home.jpg")))
      end
      produto.save
    end
  end
end


[
'Café Gourmet Brasil','Importância Sócio Ambiental','Identidade Brasileira','Franquia','Galeria de Fotos','Geração',
'Missão, Visão e Objetivo','Nossos Parceiros','Museu do Café','Localização'
].each do |titulo|
	Pagina.create(:titulo => titulo, :url => titulo.parameterize, :conteudo => Random.paragraphs(5))
end
