class CreatePedidos < ActiveRecord::Migration
  def self.up
    create_table :pedidos do |t|
      
      t.integer :tipo_cliente, :default => 0
      t.string :email
      
      
      t.string :nome_fantasia
      t.string :razao_social
      t.string :inscricao_estadual
      t.string :cnpj, :size => 20
      t.string :site
      t.string :telefone_empresa, :size => 15
      t.string :ramo_atividade

      t.string :nome
      t.string :cpf, :size => 20
      t.date :data_nascimento
      t.string :sexo, :default => 'Masculino'
      
      t.string :telefone_1, :size => 15
      t.string :telefone_2, :size => 15
      t.string :celular, :size => 15
      t.string :profissao, :size => 50

      t.string :cep, :size => 12
      t.string :endereco
      t.string :numero, :size => 10
      t.string :complemento
      t.string :bairro
      t.string :estado, :size => 2
      t.string :cidade

      t.string :entrega_cep, :size => 12
      t.string :entrega_endereco
      t.string :entrega_numero, :size => 10
      t.string :entrega_complemento
      t.string :entrega_bairro
      t.string :entrega_estado, :size => 2
      t.string :entrega_cidade

      t.float :total
      t.string :pagseguro_id
      t.string :status, :default => 'Aguardando pagamento'
      t.text :pagseguro_observacoes
      t.text :produtos
      
      
      

      t.timestamps
    end
  end

  def self.down
    drop_table :pedidos
  end
end
