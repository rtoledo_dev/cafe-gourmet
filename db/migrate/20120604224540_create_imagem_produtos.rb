class CreateImagemProdutos < ActiveRecord::Migration
  def self.up
    create_table :imagem_produtos do |t|
      t.references :produto
      t.has_attached_file :arquivo

      t.timestamps
    end
  end

  def self.down
    drop_table :imagem_produtos
  end
end
