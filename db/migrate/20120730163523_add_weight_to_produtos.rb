class AddWeightToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :peso, :float, :default => 0.3
  end

  def self.down
    remove_column :produtos, :peso
  end
end
