class CreateImagemGalerias < ActiveRecord::Migration
  def self.up
    create_table :imagem_galerias do |t|
      t.references :galeria
      t.has_attached_file :img_galeria

      t.timestamps
    end
  end

  def self.down
    drop_table :imagem_galerias
  end
end
