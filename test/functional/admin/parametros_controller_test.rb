require 'test_helper'

class Admin::ParametrosControllerTest < ActionController::TestCase
  setup do
    @admin_parametro = admin_parametros(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_parametros)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_parametro" do
    assert_difference('Admin::Parametro.count') do
      post :create, :admin_parametro => @admin_parametro.attributes
    end

    assert_redirected_to admin_parametro_path(assigns(:admin_parametro))
  end

  test "should show admin_parametro" do
    get :show, :id => @admin_parametro.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @admin_parametro.to_param
    assert_response :success
  end

  test "should update admin_parametro" do
    put :update, :id => @admin_parametro.to_param, :admin_parametro => @admin_parametro.attributes
    assert_redirected_to admin_parametro_path(assigns(:admin_parametro))
  end

  test "should destroy admin_parametro" do
    assert_difference('Admin::Parametro.count', -1) do
      delete :destroy, :id => @admin_parametro.to_param
    end

    assert_redirected_to admin_parametros_path
  end
end
